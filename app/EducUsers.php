<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducUsers extends Model
{
	protected $table = 'users';
	protected $fillable = ['UserID', 'UserName', 'PassWord', 'Admin',];
	public $timestamps = false;
    
}
