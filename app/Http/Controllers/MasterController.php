<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateLogin;
use Request;
use Validator;
use App\EducLibrary;
use App\EducUsers;
use DB;
use Session;
use App;
use View;
use Cookie;
use Mail;

use Illuminate\Routing\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class MasterController extends Controller
{

    public function login() {

        if (Session::has('key')) {

        $results = EducLibrary::all();

        return view('admin') -> with ('name', $results);

        }

        else {

        return view('login');

        }
    }

    public function verifier() {        

        $nameaccess = Request::get("UserName");
        $passwordaccess = Request::get("PassWord");

        if ($nameaccess != null && $passwordaccess != null) {

            $user = DB::table('users')->where('UserName', $nameaccess)->first();

            if ($user != null) {

                if ($passwordaccess == $user -> PassWord) {

                Session::put('key', $user -> UserName);
                Session::put('admin', $user -> Admin);
                Session::put('userid', $user -> UserID);

                    if (Session::get('key') == DB::table('users')->where('UserName', Session::get('key'))->first() -> UserName) {

                    $results = EducLibrary::all();

                    return view('admin') -> with ('name', $results);

                    }

                }

                else {

                return view('incorrect');

                }
            }

            else {

                return view('incorrect');

            }
        }

        else {

            return view('login');

        }

    }

    public function adminpanel() {

        if (Session::has('key')) {

        $results = EducUsers::all();

        return view('admin') -> with ('name', $results);

        }

        else {

        return view('denied');

        }

    }

    public function userviewer() {

        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

               $results = EducUsers::all();

               return view('userview') -> with ('name', $results);

            }

            else {

            return view('unauth');

            }

        }

        else {

        return view('denied');

        }

    }

    public function userplacer() {

        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

            $results = DB::table('usertypes')->lists('Admin');

            $resulter = array_combine($results, $results);

            return view('useradd') -> with ('User', $resulter);

            }

            else {

            return view('unauth');

            }

        }

        else {

        return view('denied');

        }

    }

    public function usermaker() {

        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

                $users = DB::table('users')->get();

                $IDaccess = Request::get("UserID");
                $nameaccess = Request::get("UserName");

                $hits = 0;

                foreach ($users as $user) {

                    if ($user->UserID == $IDaccess || $user->UserName == $nameaccess) {

                        $hits++;

                    }
                }

                if ($hits != 0) {

                    echo"<script>alert('Username or User ID already exists');</script>";

                    $results = EducUsers::all();

                    return view('userview') -> with ('name', $results);


                }

                else {


                    $EducUsers = new EducUsers;

                    $input = Request::all();

                    $EducUsers -> UserID = $input['UserID'];
                    $EducUsers -> UserName = $input['UserName'];
                    $EducUsers -> PassWord = $input['PassWord'];
                    $EducUsers -> Admin = Request::get('User');

                    $EducUsers -> save();

                    $results = EducUsers::all();

                    return view('userview') -> with ('name', $results);
                    

                }

            }

            else {

            return view('unauth');

            }

        }

        else {

        return view('denied');

        }

    }
    
    public function useredit($id) {

        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

                $user = DB::table('users')->where('id', $id)->first();

                $field = DB::table('usertypes')->lists('Admin');

                $resulter = array_combine($field, $field);

                $Identifier = $user -> id;
                $userID = $user -> UserID;
                $userName = $user -> UserName;
                $userPass = $user -> PassWord;
                $userAdmin = $resulter;

                $data = array('Identifier' => $Identifier, 'ID' => $userID, 'NAME' => $userName, 'PASS' => $userPass, 'ADMIN' => $userAdmin);  

                return view('useredit') -> with ($data);

            }

            else {

            return view('unauth');

            }

        }

        else {

        return redirect('denied');

        }
        
    }


    public function usereditsave() {
        
        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

                $users = DB::table('users')->get();

                $input = Request::all();

                $IDaccess = $input['id'];
                $nameaccess = $input['name'];

                $hits = 0;

                foreach ($users as $user) {


                    if ($user->UserID == $IDaccess || $user->UserName == $nameaccess) {

                        $hits++;

                    }

                }

                if ($hits > 1) {

                    echo"<script>alert('Username or User ID already exists');</script>";

                    $results = EducUsers::all();

                    return view('userview') -> with ('name', $results);

                }

                else {

                $input = Request::all();

                DB::table('users')->where('id', $input['Identifier'])->update(array('UserID' => $input['id'], 'UserName' => $input['name'], 'PassWord' => $input['pass'], 'Admin' => $input['admin']));

                $results = EducUsers::all();

                return view('userview') -> with ('name', $results);

                }

            }

            else {

            return view('unauth');

            }

        }

        else {

        return rediect('denied');

        }

    }

    public function userdelete($id) {

        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

                $user = DB::table('users')->where('id', $id)->first();

                $Identifier = $user -> id;
                $userID = $user -> UserID;
                $userName = $user -> UserName;
                $userPass = $user -> PassWord;
                $userAdmin = $user -> Admin;

                $data = array('Identifier' => $Identifier, 'ID' => $userID, 'NAME' => $userName, 'PASS' => $userPass, 'ADMIN' => $userAdmin);  

                return view('userdelete') -> with ($data);

            }

            else {

            return view('unauth');

            }

        }

        else {

        return redirect('denied');

        }
        
    }

    public function userdeletesave() {
        
        if (Session::has('key')) {

            if (session()->get('admin') == "Yes") {

                $input = Request::all();

                if (session()->get('key') != $input['name']) {

                DB::table('users')->where('id', $input['Identifier'])->delete();

                $results = EducUsers::all();

                return view('userview') -> with ('name', $results);

                }

                else {

                echo"<script>alert('Cannot Delete Self!');</script>";

                $results = EducUsers::all();

                return view('userview') -> with ('name', $results);

                }

            }

            else {

            return view('unauth');

            }

        }

        else {

        return rediect('denied');

        }

    }

    public function dataviewer() {

        if (Session::has('key')) {

        //$results = EducLibrary::all();

        $results = EducLibrary::paginate(15);

        return view('dataview') -> with ('name', $results);

        }

        else {

        return view('denied');

        }

    }

    public function dataplacer() {

        if (Session::has('key')) {

        $results = DB::table('areatypes')->lists('Area');

        $resulter = array_combine($results, $results);

        $languager = DB::table('languages')->lists('LanguageCode');

        $resulter2 = array_combine($languager, $languager);

        $typer = DB::table('thesistype')->lists('Type');

        $resulter3 = array_combine($typer, $typer);

        $data = array ('Area' => $resulter, 'AUTHOR' => null, 'TITLE' => null, 'YEAR' => null, 'ADVISERS' => null, 'LANGUAGE' => null, 'LANGUAGESELECT' => $resulter2, 'THESISTYPE' => $resulter3, 'ACCESSIONNUMBER' => null, 'SUBJECTS' => null, 'ABSTRACTS' => null, 'PHYSICALDESCRIPTION' => null, 'GENERALNOTE' => null);


        return view('dataadd') -> with ($data);

        }

        else {

        return view('denied');

        }

    }

public function datasearcher() {

        if (Session::has('key')) {

        $input = Request::all();

        $parameter = preg_replace('/[()*_{}|=+;:]/', '', $input['SearchType']);
        $keymaker = preg_replace('/[()*_{}|=+;:]/', '', $input['SearchKey']);

            if ($parameter == "Browse All") {

            //$results = EducLibrary::all();

            $results = EducLibrary::paginate(15);

            return view('dataview') -> with ('name', $results);

            }

            else if ($parameter == "Accession Number") {

            $parameter = "AccessionNumber";

            //$results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

            $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> paginate(15);

            return view('dataview') -> with ('name', $results);

            }

            else {

            //$results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

            $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> paginate(15);
            
            return view('dataview') -> with ('name', $results);

            }

        }

        else {

        return view('denied');

        }

    }




    public function datasearchnext() {

        if (Session::has('key')) {

            if(Request::has('SearchType')) {

                if (Request::get('SearchType') != "Browse All") {

                $parameter = preg_replace('/[()*_{}|=+;:]/', '', Request::get('SearchType'));
                $keymaker = preg_replace('/[()*_{}|=+;:]/', '', Request::get('SearchKey'));

                Session::put('SearcherType', $parameter);
                Session::put('SearcherKey', $keymaker);

                }

                else {

                Session::forget('SearcherType');
                Session::forget('SearcherKey');

                $results = EducLibrary::paginate(15);

                return view('dataview') -> with ('name', $results);

                }

                /*if(session()->get('SearcherKey') == null) {

                $parameter = preg_replace('/[()*_{}|=+;:]/', '', $input['SearchType']);
                $keymaker = preg_replace('/[()*_{}|=+;:]/', '', $input['SearchKey']);

                Session::put('SearcherType', $parameter);
                Session::put('SearcherKey', $keymaker);

                }

                else {

                $parameter = session()->get('SearcherType');
                $keymaker = session()->get('SearcherKey');

                }*/


            }

            else {

            $parameter = session()->get('SearcherType');
            $keymaker = session()->get('SearcherKey');

            }

            if ($parameter == "Browse All") {

            //$results = EducLibrary::all();

            $results = EducLibrary::paginate(15);

            return view('dataview') -> with ('name', $results);

            }

            else if ($parameter == "Accession Number") {

            $parameter = "AccessionNumber";

            //$results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

            $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> paginate(15);

            return view('dataview') -> with ('name', $results);

            }

            else {

            //$results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

            $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> paginate(15);
            
            return view('dataview') -> with ('name', $results);

            }

        }

        else {

        return view('denied');

        }

    }




    public function datamaker() {

        if (Session::has('key')) {

        $EducLibrary = new EducLibrary;

        $input = Request::all();

        $validator = Validator::make($input, [

            'Year' => 'integer',

        ]);

        if ($validator->fails()) {

            echo"<script>alert('Please fill out all the fields with the appropriate value...');</script>";

            $dbarea = DB::table('areatypes')->lists('Area');

            $dbareas = array_combine($dbarea, $dbarea);

            $languager = DB::table('languages')->lists('LanguageCode');

            $resulter2 = array_combine($languager, $languager);

            $typer = DB::table('thesistype')->lists('Type');

            $resulter3 = array_combine($typer, $typer);

            $data = array('Area' => $dbareas, 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['Language'], 'LANGUAGESELECT' => $resulter2, 'THESISTYPE' => $resulter3, 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['GeneralNote']);

            return view('dataadd') -> with($data);

        }

        //

        $hits = 0;

        $Edux = DB::table('thesisdata')->get();

        foreach ($Edux as $Educ) {

            if ($Educ->Title == $input['Title'] || $Educ->AccessionNumber == $input['AccessionNumber']) {

                $hits++;

            }
        }

        //

            if ($hits != 0) {

            echo"<script>alert('Either the Thesis Title or Accession Number is a duplicate entry or blank, please put values in those fields that are neither blank or a duplicate of a previous entry...');</script>";

            $dbarea = DB::table('areatypes')->lists('Area');

            $dbareas = array_combine($dbarea, $dbarea);

            $languager = DB::table('languages')->lists('LanguageCode');

            $resulter2 = array_combine($languager, $languager);

            $typer = DB::table('thesistype')->lists('Type');

            $resulter3 = array_combine($typer, $typer);

            $data = array('Area' => $dbareas, 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['Language'], 'LANGUAGESELECT' => $resulter2, 'THESISTYPE' => $resulter3, 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['GeneralNote']);

            return view('dataadd') -> with($data);
            
            }

            else {

                if ($input['LanguageSelect'] != "Other" && $input['ThesisType'] != "Other") {

                $data = array('AREA' => $input['Area'], 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['LanguageSelect'], 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'ID' => Session::get('userid'), 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['ThesisType']);

                return view('dataaddconfirm') -> with ($data);

                }

                else if ($input['LanguageSelect'] != "Other" && $input['ThesisType'] == "Other") {

                $data = array('AREA' => $input['Area'], 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['LanguageSelect'], 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'ID' => Session::get('userid'), 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['GeneralNote']);

                return view('dataaddconfirm') -> with ($data);

                }

                else if ($input['LanguageSelect'] == "Other" && $input['ThesisType'] != "Other") {

                $data = array('AREA' => $input['Area'], 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['Language'], 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'ID' => Session::get('userid'), 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['ThesisType']);

                return view('dataaddconfirm') -> with ($data);

                }

                else {

                $data = array('AREA' => $input['Area'], 'AUTHOR' => $input['Author'], 'TITLE' => $input['Title'], 'YEAR' => $input['Year'], 'ADVISERS' => $input['Advisers'], 'LANGUAGE' => $input['Language'], 'ACCESSIONNUMBER' => $input['AccessionNumber'], 'SUBJECTS' => $input['Subjects'], 'ABSTRACTS' => $input['Abstracts'], 'ID' => Session::get('userid'), 'PHYSICALDESCRIPTION' => $input['PhysicalDescription'], 'GENERALNOTE' => $input['GeneralNote']);

                return view('dataaddconfirm') -> with ($data);

                }

            }

        }

        else {

        return view('denied');

        }

    }

    public function dataaddconfirm() {

        if (Session::has('key')) {

        $EducLibrary = new EducLibrary;

        $input = Request::all();

        $EducLibrary -> Area = Request::get('area');
        $EducLibrary -> Author = $input['author'];
        $EducLibrary -> Title = $input['title'];
        $EducLibrary -> Year = $input['year'];
        $EducLibrary -> Advisers = $input['advisers'];
        $EducLibrary -> Language = $input['language'];
        $EducLibrary -> AccessionNumber = $input['accessionnumber']; 
        $EducLibrary -> Subjects = $input['subjects'];
        $EducLibrary -> Abstracts = $input['abstracts'];
        $EducLibrary -> PhysicalDescription = $input['physicaldescription'];
        $EducLibrary -> GeneralNote = $input['generalnote'];
        $EducLibrary -> UserID = Session::get('userid');

        $EducLibrary -> save();

        $results = EducLibrary::all();

        return redirect('dataview') -> with ('name', $results);

        }

        else {

        return rediect('denied');

        }

    }

        public function dataedit($id) {

        if (Session::has('key')) {

            $thesis = DB::table('thesisdata')->where('id', $id)->first();

            $field = DB::table('areatypes')->lists('Area');

            $resulter = array_combine($field, $field);

            $languager = DB::table('languages')->lists('LanguageCode');

            $resulter2 = array_combine($languager, $languager);

            $typer = DB::table('thesistype')->lists('Type');

            $resulter3 = array_combine($typer, $typer);

            $Selected = $thesis -> Area;
            $Identifier = $thesis -> id;
            $Area = $resulter;
            $Author = $thesis -> Author;
            $Title = $thesis -> Title;
            $Year = $thesis -> Year;
            $Advisers = $thesis -> Advisers;
            
            $Lang = DB::table('languages')->get();

            $Hits = 0;

            foreach ($Lang as $lang) {

                if ($lang -> LanguageCode == $thesis -> Language && $thesis -> Language != "Other") {

                    $Hits++;

                }
            }

            if ($Hits != 0) {

            $Language = $thesis -> Language;


            }

            else {

            $Language = "Other";

            }

            $Type = DB::table('thesistype')->get();

            $Hits2 = 0;

                foreach ($Type as $type) {

                    if ($type -> Type == $thesis -> GeneralNote && $thesis -> GeneralNote != "Other") {

                    $Hits2++;

                    }

                }

                if ($Hits2 != 0) {

                $GeneralNote = $thesis -> GeneralNote;

                }

                else {

                $GeneralNote = "Other";

                }

            $LanguageSelect = $resulter2;
            $Languager = ($thesis -> Language);

            $TypeSelect = $resulter3;
            $Typer = ($thesis -> GeneralNote);


            $AccessionNumber = $thesis -> AccessionNumber;
            $Subjects = $thesis -> Subjects;
            $Abstracts = $thesis -> Abstracts;
            $PhysicalDescription = $thesis -> PhysicalDescription;
            //$GeneralNote = $thesis -> GeneralNote;
            $UserID = Session::get('userid');

            $data = array ('Hits' => $Hits, 'Hits2' => $Hits2, 'Identifier' => $Identifier, 'AREA' => $Area, 'SELECTED' => $Selected, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'LANGUAGESELECT' => $LanguageSelect, 'SELECTED2' => $Language, 'LANGUAGER' => $Languager, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote, 'TYPESELECT' => $TypeSelect, 'SELECTED3' => $GeneralNote, 'TYPER' => $Typer);

            return view('dataedit') -> with ($data);

            }

        else {

        return view('denied');

        }

    }

    public function dataeditsave() {

        if (Session::has('key')) {

        $input = Request::all();

            $validator = Validator::make($input, [

            'year' => 'integer',

            ]);

            if ($validator->fails()) {

            echo"<script>alert('Please fill out all the fields with the appropriate value...');</script>";;

            $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

            $field = DB::table('areatypes')->lists('Area');

            $resulter = array_combine($field, $field);

            $languager = DB::table('languages')->lists('LanguageCode');

            $resulter2 = array_combine($languager, $languager);

            $typer = DB::table('thesistype')->lists('Type');

            $resulter3 = array_combine($typer, $typer);

            $Selected = $thesis -> Area;
            $Identifier = $thesis -> id;
            $Area = $resulter;
            $Author = $thesis -> Author;
            $Title = $thesis -> Title;
            $Year = $thesis -> Year;
            $Advisers = $thesis -> Advisers;
            
            $Lang = DB::table('languages')->get();

            $Hits = 0;

                foreach ($Lang as $lang) {

                    if ($lang -> LanguageCode == $thesis -> Language && $thesis -> Language != "Other") {

                        $Hits++;

                    }
                }

                if ($Hits != 0) {

                $Language = $thesis -> Language;


                }

                else {

                $Language = "Other";

                }

            $Type = DB::table('thesistype')->get();

            $Hits2 = 0;

                foreach ($Type as $type) {

                    if ($type -> Type == $thesis -> GeneralNote && $thesis -> GeneralNote != "Other") {

                    $Hits2++;

                    }

                }

                if ($Hits2 != 0) {

                $GeneralNote = $thesis -> GeneralNote;

                }

                else {

                $GeneralNote = "Other";

            }

            $LanguageSelect = $resulter2;
            $Languager = ($thesis -> Language);

            $TypeSelect = $resulter3;
            $Typer = ($thesis -> GeneralNote);

            $AccessionNumber = $thesis -> AccessionNumber;
            $Subjects = $thesis -> Subjects;
            $Abstracts = $thesis -> Abstracts;
            $PhysicalDescription = $thesis -> PhysicalDescription;
            //$GeneralNote = $thesis -> GeneralNote;
            $UserID = Session::get('userid');

            $data = array ('Hits' => $Hits, 'Hits2' => $Hits2, 'Identifier' => $Identifier, 'AREA' => $Area, 'SELECTED' => $Selected, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'LANGUAGESELECT' => $LanguageSelect, 'SELECTED2' => $Language, 'LANGUAGER' => $Languager, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote, 'TYPESELECT' => $TypeSelect, 'SELECTED3' => $GeneralNote, 'TYPER' => $Typer);

            return view('dataedit') -> with ($data);

            }

        $hits = 0;

        $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

            if ($thesis -> Title != $input['title']) {

            $Edux = DB::table('thesisdata')->get();

                foreach ($Edux as $Educ) {

                    if ($Educ->Title == $input['title']) {

                    $hits++;

                    }
                }
            }

            if ($thesis -> AccessionNumber != $input['accessionnumber']) {

            $Edux = DB::table('thesisdata')->get();

                foreach ($Edux as $Educ) {

                    if ($Educ->AccessionNumber == $input['accessionnumber']) {

                    $hits++;

                    }
                }
            }

            if ($hits != 0) {

            echo"<script>alert('Either the Thesis Title or Accession Number is a duplicate entry or blank, please put values in those fields that are neither blank or a duplicate of a previous entry...');</script>";

            $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

            $field = DB::table('areatypes')->lists('Area');

            $resulter = array_combine($field, $field);

            $languager = DB::table('languages')->lists('LanguageCode');

            $resulter2 = array_combine($languager, $languager);

            $typer = DB::table('thesistype')->lists('Type');

            $resulter3 = array_combine($typer, $typer);

            $Selected = $thesis -> Area;
            $Identifier = $thesis -> id;
            $Area = $resulter;
            $Author = $thesis -> Author;
            $Title = $thesis -> Title;
            $Year = $thesis -> Year;
            $Advisers = $thesis -> Advisers;
            
            $Lang = DB::table('languages')->get();

            $Hits = 0;

                foreach ($Lang as $lang) {

                    if ($lang -> LanguageCode == $thesis -> Language && $thesis -> Language != "Other") {

                    $Hits++;

                    }
                }

                if ($Hits != 0) {

                $Language = $thesis -> Language;

                }

                else {

                $Language = "Other";

                }

            $Type = DB::table('thesistype')->get();

            $Hits2 = 0;

                foreach ($Type as $type) {

                    if ($type -> Type == $thesis -> GeneralNote && $thesis -> GeneralNote != "Other") {

                    $Hits2++;

                    }

                }

                if ($Hits2 != 0) {

                $GeneralNote = $thesis -> GeneralNote;

                }

                else {

                $GeneralNote = "Other";

            }

            $LanguageSelect = $resulter2;
            $Languager = ($thesis -> Language);

            $TypeSelect = $resulter3;
            $Typer = ($thesis -> GeneralNote);

            $AccessionNumber = $thesis -> AccessionNumber;
            $Subjects = $thesis -> Subjects;
            $Abstracts = $thesis -> Abstracts;
            $PhysicalDescription = $thesis -> PhysicalDescription;
            //$GeneralNote = $thesis -> GeneralNote;
            $UserID = Session::get('userid');

            $data = array ('Hits' => $Hits, 'Hits2' => $Hits2, 'Identifier' => $Identifier, 'AREA' => $Area, 'SELECTED' => $Selected, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'LANGUAGESELECT' => $LanguageSelect, 'SELECTED2' => $Language, 'LANGUAGER' => $Languager, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote, 'TYPESELECT' => $TypeSelect, 'SELECTED3' => $GeneralNote, 'TYPER' => $Typer);

            return view('dataedit') -> with ($data);
            
            }

            else {

            $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

            $Identifier = $thesis -> id;
            $Area = $thesis -> Area;
            $Author = $thesis -> Author;
            $Title = $thesis -> Title;
            $Year = $thesis -> Year;

            $Advisers = $thesis -> Advisers;

                if($input['LanguageSelect'] == "Other") {

                $Language = $input['languager'];

                }
                
                else {
                
                $Language = $input['LanguageSelect'];

                }

                if($input['TypeSelect'] == "Other") {

                $GeneralNote = $input['typer'];

                }
                
                else {
                
                $GeneralNote = $input['TypeSelect'];

                }


            
            $AccessionNumber = $thesis -> AccessionNumber;

            $Subjects = $thesis -> Subjects;
            $Abstracts = $thesis -> Abstracts;
            $UserID = $thesis -> UserID;

            $PhysicalDescription = $thesis -> PhysicalDescription;
            //$GeneralNote = $thesis -> GeneralNote;
            
            $data = array('Identifier' => $input['Identifier'], 'AREA' => $input['area'], 'AUTHOR' => $input['author'], 'TITLE' => $input['title'], 'YEAR' => $input['year'], 'ADVISERS' => $input['advisers'], 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $input['accessionnumber'], 'SUBJECTS' => $input['subjects'], 'ABSTRACTS' => $input['abstracts'], 'ID' => Session::get('userid'), 'PHYSICALDESCRIPTION' => $input['physicaldescription'], 'GENERALNOTE' => $GeneralNote);
            return view('dataeditconfirm') -> with ($data);

            
            }

        }

        else {

        return view('denied');

        }

    }

    public function dataeditconfirm() {

        if (Session::has('key')) {

            $input = Request::all();

            DB::table('thesisdata')->where('id', $input['Identifier'])->update(array('Area' => $input['area'], 'Author' => $input['author'], 'Title' => $input['title'], 'Year' => $input['year'], 'Advisers' => $input['advisers'], 'Language' => $input['language'], 'AccessionNumber' => $input['accessionnumber'], 'Subjects' => $input['subjects'], 'Abstracts' => $input['abstracts'], 'PhysicalDescription' => $input['physicaldescription'], 'GeneralNote' => $input['generalnote'], 'UserID' => Session::get('userid')));

            $results = EducLibrary::all();

            return redirect('dataview') -> with ('name', $results);

        }

        else {

        return rediect('denied');

        }

    }

    public function datadelete($id) {

        if (Session::has('key')) {

            $thesis = DB::table('thesisdata')->where('id', $id)->first();

            $Identifier = $thesis -> id;
            $Area = $thesis -> Area;
            $Author = $thesis -> Author;
            $Title = $thesis -> Title;
            $Year = $thesis -> Year;

            $Advisers = $thesis -> Advisers;
            $Language = $thesis -> Language;
            $AccessionNumber = $thesis -> AccessionNumber;

            $Subjects = $thesis -> Subjects;
            $Abstracts = $thesis -> Abstracts;
            $UserID = $thesis -> UserID;

            $PhysicalDescription = $thesis -> PhysicalDescription;
            $GeneralNote = $thesis -> GeneralNote;

            $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

            return view('datadelete') -> with ($data);

        }

        else {

        return redirect('denied');

        }
        
    }

    public function datadeletesave() {
        
        if (Session::has('key')) {

                $input = Request::all();

                DB::table('thesisdata')->where('id', $input['Identifier'])->delete();

                $results = EducLibrary::all();

                return view('dataview') -> with ('name', $results);

        }

        else {

        return rediect('denied');

        }

    }

    public function userfacestart() {

        Session::forget('matchhits');

        if (Session::has('cart')) {

            Session::forget('cart');

        }

        else {

            //Session::put('cart', 'nullVal');

        }

        $field = DB::table('areatypes')->lists('Area');

        array_unshift($field, "Browse All");

        $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            //array_push($fieldcount, $items);

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
            //array_push($fieldcount, $items);

            }

        }

        //$resulter = array_combine($field, $field); //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

        $resulter = array_combine($field, $field);

        $params = DB::table('searchparameters')->lists('Parameters');

        array_unshift($params, "Browse All");

        $resulter2 = array_combine($params, $params);

        $orders = DB::table('searchparameters')->lists('Parameters');

        $deleter = array_pop($orders);
        $deleter2 = array_pop($orders);
        $deleter3 = array_pop($orders);
        $deleter4 = array_pop($orders);

        $resulter3 = array_combine($orders, $orders);

        $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

        return view('userface1') -> with ($composite);

    }

    public function userface1show() {

        $field = DB::table('areatypes')->lists('Area');

        array_unshift($field, "Browse All");

        $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            //array_push($fieldcount, $items);

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
            //array_push($fieldcount, $items);

            }

        }

        $resulter = array_combine($field, $field);

        $params = DB::table('searchparameters')->lists('Parameters');

        array_unshift($params, "Browse All");

        $resulter2 = array_combine($params, $params);

        $orders = DB::table('searchparameters')->lists('Parameters');

        $deleter = array_pop($orders);
        $deleter2 = array_pop($orders);
        $deleter3 = array_pop($orders);
        $deleter4 = array_pop($orders);

        $resulter3 = array_combine($orders, $orders);

        $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

        return view('userface1') -> with ($composite);

    }

    public function userface2show() {

        $input = Request::all();

        $fielder = substr(str_replace(' Total Records', '', preg_replace('/[()*_{}|=+;:1234567890\-]/', '', $input['Area'])), 0, -1);
        $parameter = preg_replace('/[()*_{}|=+;:\-]/', '', $input['Parameters']);
        $orderer = preg_replace('/[()*_{}|=+;:\-]/', '', $input['Orders']);
        $sorter = preg_replace('/[()*_{}|=+;:\-]/', '', $input['Sorters']);

        if ($parameter == "Accession Number") {

        $parameter = "AccessionNumber";                                                                                                                                      //  //  //  //

        }

        Session::put('Area', $fielder);
        Session::put('Parameters', $parameter);
        Session::put('Orders', $orderer);
        Session::put('Sorters', $sorter);

        if ($parameter != "Year") {

            if ($parameter == "AccessionNumber") {

            $keymaker = preg_replace('/[()*_{}|=+;:]/', '', Request::get('Search'));

            Session::put('Search', $keymaker);

            }

            else {

            $keymaker = preg_replace('/[()*_{}|=+;:\%]/', '', Request::get('Search'));

            $keysplitter = explode(" ", $keymaker);

            $keyclean = array_unique($keysplitter);

            $keyshining = array_filter(array_map('trim', $keyclean));

            $keycount = count($keyshining);

                if ($keycount > 1) {

                Session::put('Search', $keymaker);

                }

                else {

                Session::put('Search', $keymaker);

                }

            }

        }

        else {

        Session::put('Search1', $input['Search1']);
        Session::put('Search2', $input['Search2']);

        }

        //!! Possible Security Breach Entry Point!! WALL ADDED BUT NOT YET OPTIMAL!!/////////////////////////////////////////////

        if ($parameter != "Year") {

        $validator = Validator::make($input, [                                                                                                                                   //  //  //  //

        ]);

        }

        else {

        $keymaker1 = Request::get('Search1');
        $keymaker2 = Request::get('Search2');

        $input = Request::all();

        $validator = Validator::make($input, [

        'Search1' => 'required|min:1',
        'Search2' => 'required|min:1',

        ]);

        }

        if ($validator->fails()) {

        $field = DB::table('areatypes')->lists('Area');

        array_unshift($field, "Browse All");

        $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

        }

        $resulter = array_combine($field, $field);

        $params = DB::table('searchparameters')->lists('Parameters');

        array_unshift($params, "Browse All");

        $resulter2 = array_combine($params, $params);

        $orders = DB::table('searchparameters')->lists('Parameters');

        $resulter3 = array_combine($orders, $orders);

        $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

        echo"<script>alert('Invalid Input Type...');</script>";

        return view('userface1') -> with ($composite);

        }

        if ($fielder == "Browse All") {

            if ($parameter == "Browse All") {

            $keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', Request::get('Search'));

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') ->paginate(15);

	            usort($results, array($this, "sorter"));

	        	}

	        	else {

	        	$results = DB::table('thesisdata') -> orderBy($orderer) ->paginate(15);

	        	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            else if ($parameter == "Year") {

            $keymaker1 = Request::get('Search1');
            $keymaker2 = Request::get('Search2');

            $keycode = [$keymaker1, $keymaker2];

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> whereBetween($parameter, $keycode) -> get();

	            usort($results, array($this, "sorter"));

	            }

		        else {

	            $results = DB::table('thesisdata') -> whereBetween($parameter, $keycode) -> orderBy($orderer) -> get();

	        	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            else {

            	if ($orderer == "Title") {

		        $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

		        usort($results, array($this, "sorter"));

		        }

		        else {

		        $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();	

		        }

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($smartresults);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }
            }
        }

        else {

            if ($parameter == "Browse All") {

            $keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', Request::get('Search'));

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> where('Area', $fielder) -> get();

	            usort($results, array($this, "sorter"));

	        	}

	        	else {

	        	$results = DB::table('thesisdata') -> where('Area', $fielder) -> orderBy($orderer) -> get();

	        	}


                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            if ($parameter == "Year") {

                $keymaker1 = Request::get('Search1');
                $keymaker2 = Request::get('Search2');            

                $keycode = [$keymaker1, $keymaker2];

                if ($orderer == "Title") {

                $results = DB::table('thesisdata') -> where('Area', $fielder) -> whereBetween($parameter, $keycode) -> get();

                usort($results, array($this, "sorter"));

                }

                else {

                $results = DB::table('thesisdata') -> where('Area', $fielder) -> whereBetween($parameter, $keycode) -> orderBy($orderer) -> get();

            	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

		        for ($i = 0; $i < $fieldcount; $i++) {

		            if ($field[$i] != "Browse All") {

		            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
		            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

		            }

		            else {

		            $item = DB::table('thesisdata') -> count();
		            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

		            }

		        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }  

            }

            if ($parameter != "Browse All" || $parameter != "Year") {

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$keymaker%") -> get();

	            usort($results, array($this, "sorter"));

	        	}

	        	else {

	        	$results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();

	        	}

                //$results = DB::table('thesisdata') -> where('Area', Request::get('Area')) -> where($parameter, 'LIKE', "%$keymaker%") ->paginate(3);

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    //return view('userface2') -> with ('name', $smartresults);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    //$reverseComposite = array_reverse($smartresults);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');                                                             //  //  // Fielder!!! // // // // // // // // // //

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            //array_push($fieldcount, $items);

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
            //array_push($fieldcount, $items);

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

        }

    }


    public function userfaceprintshow($id) {

        $thesis = DB::table('thesisdata')->where('id', $id)->first();

        $Identifier = $thesis -> id;
        $Area = $thesis -> Area;
        $Author = $thesis -> Author;
        $Title = $thesis -> Title;
        $Year = $thesis -> Year;

        $Advisers = $thesis -> Advisers;
        $Language = $thesis -> Language;
        $AccessionNumber = $thesis -> AccessionNumber;

        $Subjects = $thesis -> Subjects;
        $Abstracts = $thesis -> Abstracts;
        $UserID = $thesis -> UserID;

        $PhysicalDescription = $thesis -> PhysicalDescription;
        $GeneralNote = $thesis -> GeneralNote;

        $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

        return view('userfaceprint') -> with ($data);
        
    }

    public function usercartadd() {

        $input = Request::all();

        $additor = (array_keys($input));

        array_shift($additor);

        $resulters = array();

        $hits = 0;

        $matchhits = 0;

        if (Session::has('cart')) {

            foreach ($additor as $add) {

                if (in_array($add, Session::get('cart'))) {

                $matchhits++;

                $hits++;

                }

                else {

                $entry = DB::table('thesisdata') -> where('id', $additor[$hits]) -> get();

                extract($entry, EXTR_OVERWRITE);

                array_push($resulters, $entry[0]);

                Session::push('cart', $add);

                $hits++;

                }

            }

        }

        else {

            foreach ($additor as $add) {

            $entry = DB::table('thesisdata') -> where('id', $additor[$hits]) -> get();

            extract($entry, EXTR_OVERWRITE);

            array_push($resulters, $entry[0]);

            Session::push('cart', $add);

            $hits++;

            }

        }

        Session::put('matchhits', $matchhits);

        return view('usercartadd') -> with ('name', $resulters);
        
    }

    public function usercartshow() {

        $results = array();

        //if (Session::get('cart') != "nullVal" && Session::get('cart') != null) {

        if (Session::get('cart') != null) {

        $matches = Session::get('cart');

        foreach ($matches as $match) {

            $entry = DB::table('thesisdata') -> where('id', $match) -> get();

            extract($entry, EXTR_OVERWRITE);

            array_push($results, $entry[0]);

        }

        return view('usercart') -> with ('name', $results);

        }

        else {

        return view('usercart') -> with ('name', $results);

        }
        
    }

    public function usercartsubtract() {

        //if (Session::get('cart') != "nullVal" && Session::get('cart') != null) {

        if (Session::get('cart') != null) {

            $input = Request::all();

            $subtractors = (array_keys($input));

            array_shift($subtractors);

            $subtractee = Session::get('cart');

            Session::put('cart', array_diff(Session::get('cart'), $subtractors));

            $results = array();

            if (Session::has('cart')) {

            $matches = Session::get('cart');

            foreach ($matches as $match) {

                $entry = DB::table('thesisdata') -> where('id', $match) -> get();

                extract($entry, EXTR_OVERWRITE);

                array_push($results, $entry[0]);

            }

            return view('usercart') -> with ('name', $results);

            }

            else {

            return view('usercart') -> with ('name', $results);

            }

        }

        else {

            $results = array();

            return view('usercart') -> with ('name', $results);

        }
        
    }

    public function clearcartexecute() {

        if (Session::has('cart')) {

            Session::forget('cart');

        }

        else {

            //Session::put('cart', 'nullVal');

        }

        return view('clearcart');
        
    }

    public function usercartadddetailsshow($id) {

        $thesis = DB::table('thesisdata')->where('id', $id)->first();

        $Identifier = $thesis -> id;
        $Area = $thesis -> Area;
        $Author = $thesis -> Author;
        $Title = $thesis -> Title;
        $Year = $thesis -> Year;

        $Advisers = $thesis -> Advisers;
        $Language = $thesis -> Language;
        $AccessionNumber = $thesis -> AccessionNumber;

        $Subjects = $thesis -> Subjects;
        $Abstracts = $thesis -> Abstracts;
        $UserID = $thesis -> UserID;

        $PhysicalDescription = $thesis -> PhysicalDescription;
        $GeneralNote = $thesis -> GeneralNote;

        $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

        return view('usercartadddetails') -> with ($data);
        
    }

    public function usercartdetailsshow($id) {

        $thesis = DB::table('thesisdata')->where('id', $id)->first();

        $Identifier = $thesis -> id;
        $Area = $thesis -> Area;
        $Author = $thesis -> Author;
        $Title = $thesis -> Title;
        $Year = $thesis -> Year;

        $Advisers = $thesis -> Advisers;
        $Language = $thesis -> Language;
        $AccessionNumber = $thesis -> AccessionNumber;

        $Subjects = $thesis -> Subjects;
        $Abstracts = $thesis -> Abstracts;
        $UserID = $thesis -> UserID;

        $PhysicalDescription = $thesis -> PhysicalDescription;
        $GeneralNote = $thesis -> GeneralNote;

        $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

        return view('usercartdetails') -> with ($data);
        
    }

    public function userfacerecall() {

        $fielder = preg_replace('/[()*_{}|=+;:\-]/', '', Session::get('Area'));
        $parameter = preg_replace('/[()*_{}|=+;:\-]/', '', Session::get('Parameters'));
        $orderer = preg_replace('/[()*_{}|=+;:\-]/', '', Session::get('Orders'));
        $sorter = preg_replace('/[()*_{}|=+;:\-]/', '', Session::get('Sorters'));


        if ($parameter != "Year") {

            //$search = preg_replace('/[()*_{}|=+;:\-]/', '', Session()->get('Search'));

            if ($parameter == "AccessionNumber") {

            $search = preg_replace('/[()*_{}|=+;:]/', '', Session()->get('Search'));

            }

            else {

            $search = preg_replace('/[()*_{}|=+;:\%]/', '', Session()->get('Search')); //                                                                                          //  //  //  //

            }

        }

        else {

            $search1 = preg_replace('/[()*_{}|=+;:\-]/', '', Session()->get('Search1'));
            $search2 = preg_replace('/[()*_{}|=+;:\-]/', '', Session()->get('Search2'));

        }

        if ($fielder == "Browse All") {

            if ($parameter == "Browse All") {

            //remove selected special characters
            $keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', $search);

            //$results = DB::table('thesisdata') -> orderBy($orderer) -> get();

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> orderBy($orderer) -> get();

	            usort($results, array($this, "sorter"));

	        	}

	        	else {

	        	$results = DB::table('thesisdata') -> orderBy($orderer) -> get();

	        	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

		        for ($i = 0; $i < $fieldcount; $i++) {

		            if ($field[$i] != "Browse All") {

		            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

		            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
		            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

		            //array_push($fieldcount, $items);

		            }

		            else {

		            $item = DB::table('thesisdata') -> count();
		            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
		            //array_push($fieldcount, $items);

		            }

		        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            else if ($parameter == "Year") {

            $keymaker1 = $search1;
            $keymaker2 = $search2;

            $keycode = [$keymaker1, $keymaker2];

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> whereBetween($parameter, $keycode) -> get();

	            usort($results, array($this, "sorter"));

	            }

	            else {

	            $results = DB::table('thesisdata') -> whereBetween($parameter, $keycode) -> orderBy($orderer) -> get();

	        	}

                if ($results != null) {

                if ($sorter == "Ascending") {

                return view('userface2') -> with ('name', $results);

                }

                else {

                $reverseComposite = array_reverse($results);

                return view('userface2') -> with ('name', $reverseComposite);

                }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            else {

            //remove selected special characters
            //$keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', $search);                   //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  

            $keymaker = $search;

            /*

            $searchTerms = explode(' ', $keymaker);

            $smartsearch = array();

            $smartresults = array();

            $searchcount = 0;

                foreach ($searchTerms as $term) {

                $term = trim($term);

                    if (!empty($term)) {

                        if ($searchcount < 3) {

                        $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$term%") -> get();
                        $smartsearch = array_merge($smartsearch, $results);
                        $searchcount++;

                        }
                    }
                }

            $smartresults = array_map("unserialize", array_unique(array_map("serialize", $smartsearch)));

            usort($smartresults, array($this, "sorter"));

            */

            //$results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();

	            if ($orderer == "Title") {

		        $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> get();

		        usort($results, array($this, "sorter"));

		        }

		        else {

		        $results = DB::table('thesisdata') -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();

		        }

            //$results = DB::table('thesisdata') -> where('Area', Request::get('Area')) -> where($parameter, 'LIKE', "%$keymaker%") ->paginate(3);

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    //return view('userface2') -> with ('name', $smartresults);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    //$reverseComposite = array_reverse($smartresults);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

        for ($i = 0; $i < $fieldcount; $i++) {

            if ($field[$i] != "Browse All") {

            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

            //array_push($fieldcount, $items);

            }

            else {

            $item = DB::table('thesisdata') -> count();
            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
            //array_push($fieldcount, $items);

            }

        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }
            }
        }

        else {

            if ($parameter == "Browse All") {

                //remove selected special characters
            $keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', $search);

            //$results = DB::table('thesisdata') -> where('Area', $fielder) -> orderBy($orderer) -> get();

	            if ($orderer == "Title") {

	            $results = DB::table('thesisdata') -> where('Area', $fielder) -> get();

	            usort($results, array($this, "sorter"));

	        	}

	        	else {

	        	$results = DB::table('thesisdata') -> where('Area', $fielder) -> orderBy($orderer) -> get();

	        	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

	        for ($i = 0; $i < $fieldcount; $i++) {

	            if ($field[$i] != "Browse All") {

	            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

	            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

	            //array_push($fieldcount, $items);

	            }

	            else {

	            $item = DB::table('thesisdata') -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
	            //array_push($fieldcount, $items);

	            }

	        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

            if ($parameter == "Year") {

                $keymaker1 = $search1;
                $keymaker2 = $search2;            

                $keycode = [$keymaker1, $keymaker2];

                if ($orderer == "Title") {

                $results = DB::table('thesisdata') -> where('Area', $fielder) -> whereBetween($parameter, $keycode) -> get();

                usort($results, array($this, "sorter"));

                }

                else {

                $results = DB::table('thesisdata') -> where('Area', $fielder) -> whereBetween($parameter, $keycode) -> orderBy($orderer) -> get();

            	}

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

	        for ($i = 0; $i < $fieldcount; $i++) {

	            if ($field[$i] != "Browse All") {

	            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

	            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

	            //array_push($fieldcount, $items);

	            }

	            else {

	            $item = DB::table('thesisdata') -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
	            //array_push($fieldcount, $items);

	            }

	        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }  

            }

            if ($parameter != "Browse All" || $parameter != "Year") {

                //remove selected special characters
                //$keymaker = preg_replace('/[()*_{}|=+;:\-]/', '', $search);// //                          //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  

                $keymaker = $search;

                /*

                $searchTerms = explode(' ', $keymaker);

                $smartsearch = array();

                $smartresults = array();

                $searchcount = 0;

                    foreach ($searchTerms as $term) {

                    $term = trim($term);

                        if (!empty($term)) {

                            if ($searchcount < 3) {

                            $results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$term%") -> get();
                            $smartsearch = array_merge($smartsearch, $results);
                            $searchcount++;

                            }
                        }
                    }

                $smartresults = array_map("unserialize", array_unique(array_map("serialize", $smartsearch)));

                usort($smartresults, array($this, "sorter"));

                */

                //$results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();

                if ($orderer == "Title") {

                $results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$keymaker%") -> get();

                usort($results, array($this, "sorter"));

            	}

            	else {

            	$results = DB::table('thesisdata') -> where('Area', $fielder) -> where($parameter, 'LIKE', "%$keymaker%") -> orderBy($orderer) -> get();

            	}

                //$results = DB::table('thesisdata') -> where('Area', Request::get('Area')) -> where($parameter, 'LIKE', "%$keymaker%") ->paginate(3);

                if ($results != null) {

                    if ($sorter == "Ascending") {

                    return view('userface2') -> with ('name', $results);

                    //return view('userface2') -> with ('name', $smartresults);

                    }

                    else {

                    $reverseComposite = array_reverse($results);

                    //$reverseComposite = array_reverse($smartresults);

                    return view('userface2') -> with ('name', $reverseComposite);

                    }

                }

                else {

                $field = DB::table('areatypes')->lists('Area');

                array_unshift($field, "Browse All");

                $fieldcount = count($field);

	        for ($i = 0; $i < $fieldcount; $i++) {

	            if ($field[$i] != "Browse All") {

	            //$item = DB::table('thesisdata') ->where($field, 'LIKE', "%$field%") -> lists('Area');

	            $item = DB::table('thesisdata') -> where('Area', 'LIKE', "%$field[$i]%") -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";

	            //array_push($fieldcount, $items);

	            }

	            else {

	            $item = DB::table('thesisdata') -> count();
	            $field[$i] = $field[$i] . " (" . $item . " Total Records)";
	            //array_push($fieldcount, $items);

	            }

	        }

                $resulter = array_combine($field, $field);

                $params = DB::table('searchparameters')->lists('Parameters');

                array_unshift($params, "Browse All");

                $resulter2 = array_combine($params, $params);

                $orders = DB::table('searchparameters')->lists('Parameters');

                $resulter3 = array_combine($orders, $orders);

                $composite = array ('Area' => $resulter, 'Parameters' => $resulter2, 'Orders' => $resulter3);

                echo"<script>alert('Your search query returned no results...');</script>";

                return view('userface1') -> with ($composite);

                }

            }

        }
        
    }


    public function logout() {

        Session::flush();
        return view('logout');

    }

    public function denied() {

        return view('denied');

    }

    public function incorrect() {

        return view('incorrect');

    }

    public function unauthorized() {

        return view('unauth');

    }

    public function send() {

        $input = Request::all();

        if (Session::get('cart') != null) {

            if ($input['Emailer'] != null) {

            $matches = Session::get('cart');

            $results = array();

                foreach ($matches as $match) {

                $entry = DB::table('thesisdata') -> where('id', $match) -> get();

                array_push($results, $entry[0]);

                }

            $data['results'] = $results;

            Mail::send('email', $data, function($message)  use ($input) {

            $message->to($input['Emailer'], 'UP College of Education Library')->subject('Thesis Information');
            
            });

            return view('sender');

            }

            else {

            $results = array();

                if (Session::get('cart') != null) {

                $matches = Session::get('cart');

                    foreach ($matches as $match) {

                    $entry = DB::table('thesisdata') -> where('id', $match) -> get();

                    extract($entry, EXTR_OVERWRITE);

                    array_push($results, $entry[0]);

                    }

                return view('usercart') -> with ('name', $results);

                }

                else {

                return view('usercart') -> with ('name', $results);

                }

            }

        }

        else {

        $results = array();

        echo"<script>alert('You have nothing in your cart');</script>";

        return view('usercart') -> with ('name', $results);

        }

    }

    public function sendsingleadd() {

        $input = Request::all();

        if ($input['Emailer'] != null) {

        $results = DB::table('thesisdata') -> where('id', $input['Identifier']) -> get();

        $data['results'] = $results;

        Mail::send('email', $data, function($message)  use ($input) {

        $message->to($input['Emailer'], 'UP College of Education Library')->subject('Thesis Information');
            
        });

        return view('sender');

        }

        else {

        $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

        $Identifier = $thesis -> id;
        $Area = $thesis -> Area;
        $Author = $thesis -> Author;
        $Title = $thesis -> Title;
        $Year = $thesis -> Year;

        $Advisers = $thesis -> Advisers;
        $Language = $thesis -> Language;
        $AccessionNumber = $thesis -> AccessionNumber;

        $Subjects = $thesis -> Subjects;
        $Abstracts = $thesis -> Abstracts;
        $UserID = $thesis -> UserID;

        $PhysicalDescription = $thesis -> PhysicalDescription;
        $GeneralNote = $thesis -> GeneralNote;

        $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

        return view('usercartadddetails') -> with ($data);

        }

    }

    public function sendsingle() {

        $input = Request::all();

        if ($input['Emailer'] != null) {

        $results = DB::table('thesisdata') -> where('id', $input['Identifier']) -> get();

        $data['results'] = $results;

        Mail::send('email', $data, function($message)  use ($input) {

        $message->to($input['Emailer'], 'UP College of Education Library')->subject('Thesis Information');
            
        });

        return view('sender');

        }

        else {

        $thesis = DB::table('thesisdata')->where('id', $input['Identifier'])->first();

        $Identifier = $thesis -> id;
        $Area = $thesis -> Area;
        $Author = $thesis -> Author;
        $Title = $thesis -> Title;
        $Year = $thesis -> Year;

        $Advisers = $thesis -> Advisers;
        $Language = $thesis -> Language;
        $AccessionNumber = $thesis -> AccessionNumber;

        $Subjects = $thesis -> Subjects;
        $Abstracts = $thesis -> Abstracts;
        $UserID = $thesis -> UserID;

        $PhysicalDescription = $thesis -> PhysicalDescription;
        $GeneralNote = $thesis -> GeneralNote;

        $data = array('Identifier' => $Identifier, 'AREA' => $Area, 'AUTHOR' => $Author, 'TITLE' => $Title, 'YEAR' => $Year, 'ADVISERS' => $Advisers, 'LANGUAGE' => $Language, 'ACCESSIONNUMBER' => $AccessionNumber, 'SUBJECTS' => $Subjects, 'ABSTRACTS' => $Abstracts, 'ID' => $UserID, 'PHYSICALDESCRIPTION' => $PhysicalDescription, 'GENERALNOTE' => $GeneralNote);

        return view('userfaceprint') -> with ($data);

        }

    }

    public function allprint() {

        $results = array();

            if (Session::get('cart') != null) {

            $matches = Session::get('cart');

                foreach ($matches as $match) {

                $entry = DB::table('thesisdata') -> where('id', $match) -> get();

                array_push($results, $entry[0]);

                }

            $data['results'] = $results;

            return view('printall') -> with ('results', $results);

            }

            else {

            return view('printall') -> with ('results', $results);

            }

    }

    public function singleadder () {

        $input = Request::get("Identifier");

        $resulters = array();

        $matchhits = 0;

        if (Session::has('cart')) {

            if (in_array($input, Session::get('cart'))) {

            $matchhits++;

            }

            else {

            $entry = DB::table('thesisdata') -> where('id', $input) -> get();

            extract($entry, EXTR_OVERWRITE);

            array_push($resulters, $entry[0]);

            Session::push('cart', $input);

            }

        }

        else {

        $entry = DB::table('thesisdata') -> where('id', $input) -> get();

        extract($entry, EXTR_OVERWRITE);

        array_push($resulters, $entry[0]);

        Session::push('cart', $input);

        }

        Session::put('matchhits', $matchhits);

        return view('usercartadd') -> with ('name', $resulters);

    }

    public function sorter($a, $b) {

        $orderer = preg_replace('/[()*_{}|=+;:\-]/', '', Session()->get('Orders'));

        $comparator1 = explode(" ", strtolower($a -> Title));
        $comparator2 = explode(" ", strtolower($b -> Title));

            if ($comparator1[0] == "a" || $comparator1[0] == "an" || $comparator1[0] == "the") {

            array_shift($comparator1);

            }

            if ($comparator2[0] == "a" || $comparator2[0] == "an" || $comparator2[0] == "the") {

            array_shift($comparator2);

            }

            if ($comparator1[0] == $comparator2[0])  { 

                return 0 ; 

            }

        return ($comparator1[0] < $comparator2[0]) ? -1 : 1; 

        /*

        if ($comparator1[0] != "A" && $comparator1[0] != "An" && $comparator1[0] != "The") {

            if ($comparator2[0] != "A" && $comparator2[0] != "An" && $comparator2[0] != "The") { 

                echo("   1   " . $comparator1[0] . "   1   " . $comparator2[0] . " | ");

                if ($comparator1[0] == $comparator2[0])  { 

                    return 0 ; 

                    }

                return ($comparator1[0] < $comparator2[0]) ? -1 : 1; 

            }

            else {

                echo("   2   " . $comparator1[0] . "   2   " . $comparator2[1] . " | ");

                if ($comparator1[0] == $comparator2[1])  { 

                    return 0 ; 

                    }

                return ($comparator1[0] < $comparator2[1]) ? -1 : 1; 

            }

        }

        else {

            if ($comparator2[0] != "A" && $comparator2[0] != "An" && $comparator2[0] != "The") {

                echo("   3   " . $comparator1[1] . "   3   " . $comparator2[0] . " | ");

                if ($comparator1[1] == $comparator2[0])  { 

                    return 0 ; 

                    }

                return ($comparator1[1] < $comparator2[0]) ? -1 : 1; 

            }

            else {

                echo("   4   " . $comparator1[1] . "   4   " . $comparator2[1] . " | ");

                if ($comparator1[1] == $comparator2[1])  { 

                    return 0 ; 

                    }

                return ($comparator1[1] < $comparator2[1]) ? -1 : 1; 

            }

        }

        */

        /*

            if ($a -> $orderer == $b -> $orderer)  { 

            return 0 ; 

            }

        return ($a -> $orderer < $b -> $orderer) ? -1 : 1;  

        */ 

    }

}
