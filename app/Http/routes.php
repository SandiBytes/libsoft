<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::group(['middleware' => ['web']], function () {
     
Route::get('/', 'MasterController@login');

Route::get('login', 'MasterController@login');

Route::post('login', 'MasterController@verifier');

Route::get('logout', 'MasterController@logout');

Route::get('denied', 'MasterController@denied');

Route::get('incorrect', 'MasterController@incorrect');

Route::get('unauth', 'MasterController@unauthorized');

Route::get('admins', 'MasterController@adminpanel');

Route::get('userview', 'MasterController@userviewer');

Route::get('useradd', 'MasterController@userplacer');

Route::post('useradd', 'MasterController@usermaker');

Route::get('useredit/{id}', 'MasterController@useredit');

Route::post('useredit', 'MasterController@usereditsave');

Route::get('userdelete/{id}', 'MasterController@userdelete');

Route::post('userdelete', 'MasterController@userdeletesave');

Route::get('dataview', 'MasterController@dataviewer');

Route::post('dataview', 'MasterController@datasearcher');

Route::get('dataadminsearch', 'MasterController@datasearchnext');

//Route::post('dataadminsearch', 'MasterController@datasearcher');

Route::get('dataadd', 'MasterController@dataplacer');

Route::post('dataadd', 'MasterController@datamaker');

Route::post('dataaddconfirm', 'MasterController@dataaddconfirm');

Route::get('dataedit/{id}', 'MasterController@dataedit');

Route::post('dataedit', 'MasterController@dataeditsave');

Route::post('dataeditconfirm', 'MasterController@dataeditconfirm');

Route::get('datadelete/{id}', 'MasterController@datadelete');

Route::post('datadelete', 'MasterController@datadeletesave');

Route::get('userface0', 'MasterController@userfacestart');

Route::get('userface1', 'MasterController@userface1show');

Route::get('userface2', 'MasterController@userface2show');

//Route::post('userface1', 'MasterController@userface2show');

//Route::get('userface2', 'MasterController@userfacerecall');

Route::post('userface2', 'MasterController@userfacerecall');

Route::get('userfaceprint/{id}', 'MasterController@userfaceprintshow');

Route::post('usercartadd', 'MasterController@usercartadd');

Route::get('usercartadddetails/{id}', 'MasterController@usercartadddetailsshow');

Route::get('usercart', 'MasterController@usercartshow');

Route::post('usercart', 'MasterController@usercartsubtract');

Route::get('usercartdetails/{id}', 'MasterController@usercartdetailsshow');

Route::get('clearcart', 'MasterController@clearcartexecute');

Route::post('email', 'MasterController@send');

Route::post('singlesender', 'MasterController@sendsingle');

Route::post('singleadder', 'MasterController@singleadder');

Route::post('singlesenderadd', 'MasterController@sendsingleadd');

Route::get('printall', 'MasterController@allprint');

//});
