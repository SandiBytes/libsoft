<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducLibrary extends Model
{
	protected $table = 'thesisdata';
	protected $fillable = ['Area', 'Author', 'Title', 'Year', 'Keywords', 'Abstract',];
	public $timestamps = false;
    
}
