<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

        <script>

            $(document).ready(function(){

            var defaultlabel = "Characters Left: ";

            var limit1 = parseInt($('#textfield1').attr('maxlength'));  
            var text1 = $('#textfield1').val();  
            var chars1 = text1.length;  

            var limit2 = parseInt($('#textfield2').attr('maxlength'));  
            var text2 = $('#textfield2').val();  
            var chars2 = text2.length;  

            var limit3 = parseInt($('#textfield3').attr('maxlength'));  
            var text3 = $('#textfield3').val();  
            var chars3 = text3.length;  

            $("#fieldtext1").text(defaultlabel + (limit1 - chars1));
            $("#fieldtext2").text(defaultlabel + (limit2 - chars2));
            $("#fieldtext3").text(defaultlabel + (limit3 - chars3));

        $(function(){
           
           $('#textfield1').keyup(function() {

            limit1 = parseInt($(this).attr('maxlength'));  
            text1 = $(this).val();  
            chars1 = text1.length;  

            $("#fieldtext1").text(defaultlabel + (limit1 - chars1));

           });

           $('#textfield2').keyup(function() {

            limit2 = parseInt($(this).attr('maxlength'));  
            text2 = $(this).val();  
            chars2 = text2.length;  

            $("#fieldtext2").text(defaultlabel + (limit2 - chars2));

           });

           $('#textfield3').keyup(function() {

            limit3 = parseInt($(this).attr('maxlength'));  
            text3 = $(this).val();  
            chars3 = text3.length;  

            $("#fieldtext3").text(defaultlabel + (limit3 - chars3));

           });

        });

        });

        </script>
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Add New User
                </div>

                <div id="line">
                </div>

                {!! Form::open(array('action' => 'MasterController@usermaker')) !!}

                <table class="formstyle">
                <tr>
                <td>
                <p class="designertext"></p>
                </td>
                <td>
                <p class="designertext">ID</p>
                </td>
                <td>
                <p class="designertext">Username</p>
                </td>
                <td>
                <p class="designertext">Password</p>
                </td>
                <td>
                <p class="designertext">Admin Property</p>
                </td>
                <td>
                <p class="designertext"></p>
                </td>
                <tr>
                <td>
                {!! Form::label('Input User') !!}
                </td>
                <td>
                {!! Form::text('UserID', null, array('id' => 'textfield1', 'maxlength' => 10 )) !!}<p class="labeltext" id="fieldtext1">Characters Left: 10</p>
                </td>
                <td>
                {!! Form::text('UserName', null, array('id' => 'textfield2', 'maxlength' => 20 )) !!}<p class="labeltext" id="fieldtext2">Characters Left: 20</p>
                </td>
                <td>
                {!! Form::text('PassWord', null, array('id' => 'textfield3', 'maxlength' => 20 )) !!}<p class="labeltext" id="fieldtext3">Characters Left: 20</p>
                </td>
                <td>
                {!! Form::select('User', $User) !!}
                </td>
                <td>
                {!! Form::submit('Add', ['class' => 'buttonite']) !!}
                </td>
                </tr>
                </table>

                {!! Form::close() !!}

                <a href="userview" class="buttonite">Go Back</a>

            </div>
        </div>
    </body>
</html>
