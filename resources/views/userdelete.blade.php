<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Delete User
                </div>
                <div class="windowstyle">
                    <table class="formstyle2">
                        <tr>
                        <td>
                        <p class="designertext">ID</p>
                        </td>
                        <td>
                        <p class="designertext">Name</p>
                        </td>
                        <td>
                        <p class="designertext">Password</p>
                        </td>
                        <td>
                        <p class="designertext">Admin Status</p>
                        </td>
                        <td>
                        </td>
                        </tr>
                        <tr>
                        {!! Form::open(array('action' => 'MasterController@userdeletesave')) !!}

                        <td>{!!Form::label($ID)!!}</td>
                        <td>{!!Form::label($NAME)!!}</td>
                        <td>{!!Form::label($PASS)!!}</td>
                        <td>{!!Form::label($ADMIN)!!}</td>

                        <td>{!!Form::hidden('Identifier', $Identifier)!!}{!!Form::hidden('id', $ID)!!}{!!Form::hidden('name', $NAME)!!}{!!Form::hidden('pass', $PASS)!!}{!!Form::hidden('admin', $ADMIN)!!}{!! Form::submit('Delete', ['class' => 'buttonite']) !!}</td>
                        
                        
                        {!! Form::close() !!}
                        
                        </tr>
                    </table>
                </div>

                    <a href="{{ url('userview') }}" class="buttonite">Go Back</a>
            </div>
        </div>
    </body>
</html>
