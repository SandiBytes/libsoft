<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        {!! Html::style('style.css') !!}

    </head>


    <style>

    table {
        border-spacing: 5px !important;
        border-collapse: inherit;
    }

    td {
        margin: 10px !important;
    }

    .formstyle2 td {
        vertical-align: middle !important;
    }

    .checkbox {
        display: inline !important;
    }

    </style>

    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                All Results Per Area
                </div>

                {!! Form::open(array('action' => 'MasterController@usercartadd')) !!}

                <div class="formstyle">

                <tr>

                <td><a href="userface1" class="buttonitelarge">Go Back</a></td>
                <td>{!! Form::submit('Add Checked Items to Cart', ['class' => 'buttonite']) !!}</td>
                <td><a href="usercart" class="buttonitelarge">View Cart</a></td>

                </tr>

                </div>

                <div class="windowstyle">
                    <table class="formstyle2">

                    <br>
                    <tr>
                    <?php echo $name->render(); ?>
                    </tr>

                    <hr>

                    <tr>
                    <td style="width:1px!;!important">
                    </td>
                    <td>
                    <p class="designertext">View Details</p>
                    </td>
                    <td>
                    <p class="designertext">Add to Cart</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <!--
                    <td class="firstlimiter">
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    -->
                    <td style="width:1px!;!important">
                    </td>
                    </tr>

                    @foreach ($name as $name)
                    <tr>

                    <td style="width:1px!;!important">

                    </td>
                    <td>
                    <!--<a href="#" class="buttonite">Go to Details</a>-->
                    <a href="userfaceprint/{{($name -> id)}}" class="buttonite">Details</a>

                    </td>

                    <td>

                    <div class="limiter">{!!Form::checkbox($name -> id, $name -> id, null, ['class' => 'checkbox'])!!}</div>

                    </td>

                    <td>
                    <div class="limiter">{!!Form::label($name -> Author)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label('Title', $name -> Title)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label($name -> Year)!!}</div>
                    </td>
                    <!--
                    <td>
                    <div class="thirdlimiter">{!!Form::label('abstracts', $name -> Abstracts)!!}</div>
                    </td>
                    -->
                    <td style="width:1px!;!important">
                    </td>
                    </tr>
                    @endforeach
                    </table>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </body>
</html>
