<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                User Management System
                </div>

                <a href="useradd" class="buttonite">Add User</a>
                <a href="admins" class="buttonite">Go Back</a>
                
                <div class="windowstyle">
                    <table class="formstyle2">
                    <tr>
                    <td>
                        <p class="designertext">ID</p>
                    </td>
                    <td>
                        <p class="designertext">Name</p>
                    </td>
                    <td>
                        <p class="designertext">Password</p>
                    </td>
                    <td>
                        <p class="designertext">Admin Status</p>
                    </td>
                    <td>
                        <p class="designertext">Edit</p>
                    </td>
                    <td>
                        <p class="designertext">Delete</p>
                    </td>
                    </tr>
                    @foreach ($name as $name)
                    <tr>
                    <td>
                    {!!Form::label($name -> UserID)!!}
                    </td>
                    <td>
                    {!!Form::label($name -> UserName)!!}
                    </td>
                    <td>
                    {!!Form::label($name -> PassWord)!!}
                    </td>
                    <td>
                    {!!Form::label($name -> Admin)!!}
                    </td>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="useredit/{{($name -> id)}}" class="buttonite">Edit</a>
                    </td>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="userdelete/{{($name -> id)}}" class="buttonite">Delete</a>
                    </td>
                    </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
