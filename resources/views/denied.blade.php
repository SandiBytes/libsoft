<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Please Log in to Continue
                </div>

                <hr>

                <table class="formstyle">
                <tr>
                <td>
                <a href="login" class="buttonite">Continue</a>
                </td>
                </tr>
                </table>

            </div>
        </div>
    </body>
</html>
