<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}
        
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Delete Entry
                </div>
                <div class="windowstyle">
                    <table class="formstyle2">
                        <tr>
                        <td>
                        <p class="designertext">Delete Field</p>
                        </td>
                        <td>
                        <p class="designertext">Area/Field</p>
                        </td>
                        <td>
                        <p class="designertext">Author</p>
                        </td>
                        <td>
                        <p class="designertext">Title</p>
                        </td>
                        <td>
                        <p class="designertext">Year</p>
                        </td>
                        <td>
                        <p class="designertext">Advisers</p>
                        </td>
                        <td>
                        <p class="designertext">Language</p>
                        </td>
                        <td>
                        <p class="designertext">Accession Number</p>
                        </td>
                        <td>
                        <p class="designertext">Subjects</p>
                        </td>
                        <td>
                        <p class="designertext">Abstract Snippet</p>
                        </td>
                        <td>
                        <p class="designertext">Processor ID</p>
                        </td>
                        <td>
                        </td>
                        </tr>
                        <tr>
                        {!! Form::open(array('action' => 'MasterController@datadeletesave')) !!}

                        <td>{!! Form::submit('Delete', ['class' => 'buttonite']) !!}</td>

                        <td>{!!Form::label($AREA)!!}</td>
                        <td>{!!Form::label($AUTHOR)!!}</td>
                        <td>{!!Form::label($TITLE)!!}</td>
                        <td>{!!Form::label($YEAR)!!}</td>

                        <td>{!!Form::label($ADVISERS)!!}</td>
                        <td>{!!Form::label($LANGUAGE)!!}</td>
                        <td>{!!Form::label($ACCESSIONNUMBER)!!}</td>

                        <td>{!!Form::label($SUBJECTS)!!}</td>
                        <td><div class="limiter">{!!Form::label($ABSTRACTS)!!}</div></td>
                        <td>{!!Form::label($ID)!!}</td>

                        <td>{!!Form::hidden('Identifier', $Identifier)!!}
                        {!!Form::hidden('area', $AREA)!!}
                        {!!Form::hidden('author', $AUTHOR)!!}
                        {!!Form::hidden('title', $TITLE)!!}
                        {!!Form::hidden('year', $YEAR)!!}

                        {!!Form::hidden('advisers', $ADVISERS)!!}
                        {!!Form::hidden('language', $LANGUAGE)!!}
                        {!!Form::hidden('accessionnumber', $ACCESSIONNUMBER)!!}

                        {!!Form::hidden('subjects', $SUBJECTS)!!}
                        {!!Form::hidden('abstracts', $ABSTRACTS)!!}
                        {!!Form::hidden('id', $ID)!!}</td>
                        
                        {!! Form::close() !!}
                        </tr>
                    </table>
                </div>

                    <a href="{{ url('dataview') }}" class="buttonite">Go Back</a>
            </div>
        </div>
    </body>
</html>