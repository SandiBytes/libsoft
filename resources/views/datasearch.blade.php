<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Data Management System
                </div>

                <a href="dataadd" class="buttonite">Input Data Entry</a>
                <a href="admins" class="buttonite">Go Back</a>
                
                <div class="windowstyle">
                    <table class="formstyle2">
                    <tr>
                    <td>
                    <p class="designertext">Edit</p>
                    </td>
                    <td>
                    <p class="designertext">Delete</p>
                    </td>
                    <td>
                    <p class="designertext">Area/Field</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <td>
                    <p class="designertext">Advisers</p>
                    </td>
                    <td>
                    <p class="designertext">Language</p>
                    </td>
                    <td>
                    <p class="designertext">Accession Number</p>
                    </td>
                    <td>
                    <p class="designertext">Subjects</p>
                    </td>
                    <td>
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    <td>
                    <p class="designertext">Processor ID</p>
                    </td>
                    </tr>

                    @foreach ($name as $name)
                    <tr>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="dataedit/{{($name -> id)}}" class="buttonite">Edit</a>
                    </td>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="datadelete/{{($name -> id)}}" class="buttonite">Delete</a>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Area)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Author)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Title)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Year)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Advisers)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Language)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> AccessionNumber)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Subjects)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> Abstracts)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($name -> UserID)!!}
                    </div>
                    </td>
                    </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
