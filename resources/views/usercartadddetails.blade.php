<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

        <script>
            function printThis() {
                window.print();
                //window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
            }
        </script>

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Thesis Details
                </div>

                {!! Form::open(array('action' => 'MasterController@usercartshow')) !!}
                <table class="formstyle">
                        <tr>
                        <td>{!! Form::submit('Proceed to Cart', ['class' => 'buttonite']) !!}</td>
                {!! Form::close() !!}
                        <td><a class="buttonitelarge" onclick="printThis()">Print</a></td>
                {!! Form::open(array('action' => 'MasterController@sendsingleadd')) !!}
                        <td>{!! Form::submit('Send to Email', ['class' => 'buttonite']) !!}</td>
                        <td>{!! Form::email('Emailer', null, array('id' => 'emailer')) !!}</td>
                        </tr>
                        <tr>
                </table>

                <div class="windowstyle">
                    <table class="formstyle2">
                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Title</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('title', $TITLE)!!}</div></td>
                        </tr>
                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Author</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('author', $AUTHOR)!!}</div></td>
                        </tr>
                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Year</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('year', $YEAR)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Physical Description</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('physicaldescription', $PHYSICALDESCRIPTION)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Dissertation/Thesis Note</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('generalnote', $GENERALNOTE)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Subjects</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('subjects', $SUBJECTS)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Advisers</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('advisers', $ADVISERS)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Language Codes</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('language', $LANGUAGE)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Area/Field</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('area', $AREA)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Abstract</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('abstracts', $ABSTRACTS)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext">Accession Numbers</p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::label('accessionnumber', $ACCESSIONNUMBER)!!}</div></td>
                        </tr>

                        <tr>
                        <td class="smallcolumn">
                        <p class="designertext"></p>
                        </td>
                        <td><div class="secondlimiter">{!!Form::hidden('Identifier', $Identifier)!!}</div></td>
                        </tr>
                        <tr>

                    </table>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </body>
</html>
