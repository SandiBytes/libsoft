<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
        
        
        {!! Html::style('style.css') !!}

        <script>

        $(window).load(function() {
            window.print();
        });

        function printThis() {
                window.print();
                //window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
        }

        </script>

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Print All
                </div>

                <div id="line">
                <hr>
                </div>

                <div class="row">
                <div class="col-md-2" id="formelement"><a class="buttonite" onclick="printThis()">Print All</a></div>
                <div class="col-md-2" id="formelement"><a href="{{ url('usercart') }}" class="buttonite">Go Back</a></div>
                </div>

                <div class="windowstyle">
                <div class="container-fluid">
                @foreach ($results as $results)

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Area/Field:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Area)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Author:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Author)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Title:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Title)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Year:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Year)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Advisers:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Advisers)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Language:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Language)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Accession Number:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> AccessionNumber)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Subjects:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Subjects)!!}</div>
                    </div>
                    </div>

                    <br>

                    <div class="row">
                    <div class="col-md-2" id="formelement">
                    <div class="limiter"><p class="designertext">Abstract:</p></div>
                    </div>
                    <div class="col-md-10" id="formelement">
                    <div class="thirdlimiter">{!!Form::label($results -> Abstracts)!!}</div>
                    </div>
                    </div>

                    <br>
                    <br>

                    <div id="line">
                    <hr>
                    </div>



                @endforeach

                    <p class="designertext">Thank you for using the UP College of Education Library!</p>


                </div>
                </div>
            </div>
        </div>
    </body>
</html>