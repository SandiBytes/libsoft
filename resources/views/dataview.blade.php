<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        
        {!! Html::style('style.css') !!}

    </head>

    <style>

    table {
        border-spacing: 5px !important;
        border-collapse: inherit;
    }

    td {
        margin: 10px !important;
    }

    .formstyle2 td {
        vertical-align: middle !important;
    }

    </style>

    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Data Management System
                </div>

                <div id="line">
                </div>

                <table>

                <tr>
                {!! Form::open(array('action' => 'MasterController@datasearchnext', 'method' => 'get')) !!}
                <td>
                <a href="dataadd" class="buttonite">Input Data Entry</a>
                </td>
                <td>
                <a href="admins" class="buttonite">Go Back</a>
                </td>
  
                <td>
                {!! Form::label('SearchLabel', 'Search Records By:') !!}
                </td>
                <td>
                {!! Form::select('SearchType', array('Browse All' => 'Browse All', 'UserID' => 'UserID', 'Author' => 'Author', 'Title' => 'Title', 'Year' => 'Year', 'Advisers' => 'Advisers', 'Language' => 'Language', 'Accession Number' => 'Accession Number', 'Subjects' => 'Subjects')) !!}
                </td>
                <td>
                {!! Form::text('SearchKey', '', array('id' => 'textfield', 'maxlength' => 1000 )) !!}
                </td>
                <td>
                {!! Form::submit('Search', ['class' => 'buttonitesmall']) !!}
                </td>

                {!! Form::close() !!}

                </tr>
                <!--
                <tr>
                    <form action="/search" method="GET">
                        <td>
                        <p class="designertext">Search Records By: </p>
                        </td>
                        <td>
                        <select name="searchtype">
                            <option value="Browse All">Browse All</option>
                            <option value="UserID">UserID</option>
                            <option value="Author">Author</option>
                            <option value="Title">Title</option>
                            <option value="Year">Year</option>
                            <option value="Advisers">Advisers</option>
                            <option value="Language">Language</option>
                            <option value="Accession Number">Accession Number</option>
                            <option value="Subjects">Subjects</option>
                        <select>
                        </td>
                        <td>
                        <input type="select" name="search" value=""/>
                        </td>
                        <td>
                        <button type"submit">Search</button>
                        </td>
                    </form>
                </tr>
                -->
                </table>
                
                <div class="windowstyle">
                    <table class="formstyle2">
                    <tr>
                    <br>
                    <?php echo $name->render(); ?>
                    </tr>
                    <hr>
                    <tr>
                    <td>
                    <p class="designertext">Edit</p>
                    </td>
                    <td>
                    <p class="designertext">Delete</p>
                    </td>
                    <td>
                    <p class="designertext">Area/Field</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <!--
                    <td>
                    <p class="designertext">Advisers</p>
                    </td>
                    <td>
                    <p class="designertext">Language</p>
                    </td>
                    <td>
                    <p class="designertext">Accession Number</p>
                    </td>
                    <td>
                    <p class="designertext">Subjects</p>
                    </td>
                    <td>
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    <td>
                    <p class="designertext">Processor ID</p>
                    </td>
                    -->
                    </tr>

                    @foreach ($name as $named)
                    <tr>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="dataedit/{{($named -> id)}}" class="buttonite">Edit</a>
                    </td>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="datadelete/{{($named -> id)}}" class="buttonite">Delete</a>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Area)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Author)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label('Title', $named -> Title)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Year)!!}
                    </div>
                    </td>
                    <!--
                    <td><div class="limiter">
                    {!!Form::label($named -> Advisers)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Language)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> AccessionNumber)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Subjects)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Abstracts)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> UserID)!!}
                    </div>
                    </td>
                    -->
                    </tr>
                    @endforeach
                    </table>

                </div>
            </div>
        </div>
    </body>
</html>
