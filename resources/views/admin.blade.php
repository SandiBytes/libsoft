<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Admin Panel
                </div>

                <div id="line">
                </div>

                <table class="formstyle">
                <tr>

                @if (Session::get('admin') == "Yes")

                <td>
                <a href="userview" class="buttonite">User Management</a>
                </td>

                @endif

                <td>
                <a href="dataview" class="buttonite">Data Entry</a>
                </td>
                <td>
                <a href="logout" class="buttonite">Logout</a>
                </td>
                </tr>
                </table>

            </div>
        </div>
    </body>
</html>
