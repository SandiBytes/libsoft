<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">


        <script>

        $(document).ready(function(){

        $(function(){


            $('#specialselect').change(function() {
            
            if($("#specialselect option:selected" ).text() == "Year") {


                $("#selectortext").text("Enter Year Range from Start to End");
                $("#selectornode").remove();
                $("#selectornode1").remove();
                $("#selectornode2").remove();
                $("#selectorbox").append('<input id="selectornode1" name="Search1" type="number" style="margin:5px;">');
                $("#selectorbox").append('<input id="selectornode2" name="Search2" type="number" style="margin:5px;">');

            } 

            else {

                $("#selectortext").text("Input Word To Search");
                $("#selectornode1").remove();
                $("#selectornode2").remove();
                $("#selectornode").remove();
                $("#selectorbox").append("<input id='selectornode' name='Search' type='text' style='margin:5px;'>");
            }

            });

            $('input:radio[name=Sorters]').click(function() {
            var value = $(this).val();
            $("#fieldtext").text("Sort by: " + value);
            });     

        });

        });

        </script>
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Search Our Database
                </div>

                <div id="line">
                </div>

                <div id="containerArea">

                {!! Form::open(array('action' => 'MasterController@userface2show', 'method' => 'get')) !!}
                <div class="container-fluid">
                <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::label("Select Area/Field of Study") !!}
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::select('Area', $Area) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::label("Specify Search Parameter") !!}
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::select('Parameters', $Parameters, 'default', array('id' => 'specialselect')) !!}
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::label("Order By Class") !!}
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::select('Orders', $Orders, 'default', array('id' => 'specialselect')) !!}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                <p class="labeltext" id="fieldtext">Sort by: Ascending</p>
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::radio('Sorters', 'Ascending', true) !!}
                {!! Form::radio('Sorters', 'Descending') !!}
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::label("Input Word to Search", null, array('id' => 'selectortext')) !!}
                    </div>
                    <div class="col-md-4" id="selectorbox">
                    {!! Form::text('Search', null, array('id' => 'selectornode')) !!}
                    </div>
                </div>
                 <div class="row">
                    <div class="col-md-2" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                    </div>
                    <div class="col-md-4" id="formelement">
                {!! Form::submit('Go', ['class' => 'buttoniteMega']) !!}
                    </div>
                </div>
                </div>
                {!! Form::close() !!}

                </div>

            </div>
        </div>
    </body>
</html>
