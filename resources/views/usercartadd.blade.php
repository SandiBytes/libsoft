<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                The Following Has Been Added
                </div>

                {!! Form::open(array('action' => 'MasterController@usercartshow')) !!}

                <div class="formstyle">

                <tr>

                <td><a href="userface2" class="buttonite">Confirm and Return</a></td>
                <td><a href="{{ url('usercart') }}" class="buttonite">Confirm and View Full Cart</a></td>

                </tr>

                </div>

                <div class="windowstyle">

                    @if (Session::get('matchhits') > 0) 

                    <p class="designertext">!!! SOME OR ALL OF YOUR ENTRIES HAVE NOT BEEN ADDED AS THEY HAVE ALEADY BEEN ADDED TO THE EXISTING CART !!!</p>

                    @endif

                    <table class="formstyle2">
                    <tr>
                    <td style="width:1px!;!important">
                    </td>
                    <td>
                    <p class="designertext">View Details</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <td class="firstlimiter">
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    <td style="width:1px!;!important">
                    </td>
                    </tr>

                    @foreach ($name as $name)
                    <tr>

                    <td style="width:1px!;!important">

                    </td>
                    <td>
                    <!--<a href="#" class="buttonite">Go to Details</a>-->
                    <a href="usercartadddetails/{{($name -> id)}}" class="buttonite">Details</a>

                    </td>

                    <td>
                    <div class="limiter">{!!Form::label($name -> Author)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label('Title', $name -> Title)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label($name -> Year)!!}</div>
                    </td>
                    <td>
                    <div class="thirdlimiter">{!!Form::label('abstracts', $name -> Abstracts)!!}</div>
                    </td>
                    <td style="width:1px!;!important">
                    </td>
                    </tr>
                    @endforeach
                    </table>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </body>
</html>
