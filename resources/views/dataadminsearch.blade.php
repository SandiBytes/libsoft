<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Data Management System
                </div>

                <div id="line">
                </div>

                <table>
                <tr>
                {!! Form::open(array('action' => 'MasterController@datasearcher')) !!}
                <td>
                <a href="dataadd" class="buttonite">Input Data Entry</a>
                </td>
                <td>
                <a href="admins" class="buttonite">Go Back</a>
                </td>

                <td>
                {!! Form::label('SearchLabel', 'Search Records By:') !!}
                </td>
                <td>
                {!! Form::select('SearchType', array('Browse All' => 'Browse All', 'UserID' => 'UserID', 'Author' => 'Author', 'Title' => 'Title', 'Year' => 'Year', 'Advisers' => 'Advisers', 'Language' => 'Language', 'Accession Number' => 'Accession Number', 'Subjects' => 'Subjects')) !!}
                </td>
                <td>
                {!! Form::text('SearchKey', '', array('id' => 'textfield', 'maxlength' => 1000 )) !!}
                </td>
                <td>
                {!! Form::submit('Search', ['class' => 'buttonitesmall']) !!}
                </td>

                {!! Form::close() !!}
                </tr>
                </table>
                
                <div class="windowstyle">
                    <table class="formstyle2">
                    <br>
                    <tr>
                    Some Fields Hidden to Facilitate Encoding Speed
                    <?php echo $name->render(); ?>
                    </tr>
                    <br>
                    <hr>
                    <tr>
                    <tr>

                    <tr>
                    <td>
                    <p class="designertext">Edit</p>
                    </td>
                    <td>
                    <p class="designertext">Delete</p>
                    </td>
                    <td>
                    <p class="designertext">Area/Field</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <!--
                    <td>
                    <p class="designertext">Advisers</p>
                    </td>
                    <td>
                    <p class="designertext">Language</p>
                    </td>
                    <td>
                    <p class="designertext">Accession Number</p>
                    </td>
                    <td>
                    <p class="designertext">Subjects</p>
                    </td>
                    <td>
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    <td>
                    <p class="designertext">Processor ID</p>
                    </td>
                    -->
                    </tr>

                    @foreach ($name as $named)
                    <tr>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="dataedit/{{($named -> id)}}" class="buttonite">Edit</a>
                    </td>
                    <td>
                    <!--{!! Form::submit('Edit') !!}-->
                    <a href="datadelete/{{($named -> id)}}" class="buttonite">Delete</a>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Area)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Author)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label('Title', $named -> Title)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Year)!!}
                    </div>
                    </td>
                    <!--
                    <td><div class="limiter">
                    {!!Form::label($named -> Advisers)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Language)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> AccessionNumber)!!}
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Subjects)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> Abstracts)!!}
                    </div>
                    </div>
                    </td>
                    <td><div class="limiter">
                    {!!Form::label($named -> UserID)!!}
                    </div>
                    </td>
                    -->
                    </tr>
                    @endforeach
                    </table>

                </div>
            </div>
        </div>
    </body>
</html>
