<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Login
                </div>

                <div id="line">
                </div>

                {!! Form::open(array('action' => 'MasterController@verifier')) !!}

                <table class="formstyle">
                <tr>
                <td>
                {!! Form::label('Enter User Name') !!}
                </td>
                <td>
                {!! Form::text('UserName') !!}
                </td>
                </tr>
                <tr>
                <td>
                {!! Form::label('Enter Password') !!}
                </td>
                <td>
                {!! Form::password('PassWord') !!}
                </td>
                </tr>
                <tr>
                <td colspan="2">
                {!! Form::submit('Submit', ['class' => 'buttonite']) !!}
                </td>
                </tr>
                </table>
                {!! Form::close() !!}

            </div>
        </div>
    </body>
</html>
