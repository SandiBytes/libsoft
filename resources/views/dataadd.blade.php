<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <script>

        $(document).ready(function(){

            var defaultlabel = "Characters Left: ";

            var arealimit = parseInt($('#areatextfield1').attr('maxlength'));
            var areatext = $('#areatextfield1').val();  
            var areachars = areatext.length;  

            var limit1 = parseInt($('#textfield1').attr('maxlength'));  
            var text1 = $('#textfield1').val();  
            var chars1 = text1.length;  

            var limit2 = parseInt($('#textfield2').attr('maxlength'));  
            var text2 = $('#textfield2').val();  
            var chars2 = text2.length;  

            var limit3 = parseInt($('#textfield3').attr('maxlength'));  
            var text3 = $('#textfield3').val();  
            var chars3 = text3.length;  

            var limit4 = parseInt($('#textfield4').attr('maxlength'));  
            var text4 = $('#textfield4').val();  
            var chars4 = text4.length;  

            var limit5 = parseInt($('#textfield5').attr('maxlength'));  
            var text5 = $('#textfield5').val();  
            var chars5 = text5.length; 

            var limit6 = parseInt($('#textfield6').attr('maxlength'));  
            var text6 = $('#textfield6').val();  
            var chars6 = text6.length;  

            var limit7 = parseInt($('#textfield7').attr('maxlength'));  
            var text7 = $('#textfield7').val();  
            var chars7 = text7.length;  

            var limit8 = parseInt($('#textfield8').attr('maxlength'));  
            var text8 = $('#textfield8').val();  
            var chars8 = text8.length;  

            var limit9 = parseInt($('#textfield9').attr('maxlength'));  
            var text9 = $('#textfield9').val();  
            var chars9 = text9.length; 

            $("#areatext1").text(defaultlabel + (arealimit - areachars));
            $("#fieldtext1").text(defaultlabel + (limit1 - chars1));
            $("#fieldtext2").text(defaultlabel + (limit2 - chars2));
            $("#fieldtext3").text(defaultlabel + (limit3 - chars3));
            $("#fieldtext4").text(defaultlabel + (limit4 - chars4));
            $("#fieldtext5").text(defaultlabel + (limit5 - chars5));
            $("#fieldtext6").text(defaultlabel + (limit6 - chars6));
            $("#fieldtext7").text(defaultlabel + (limit7 - chars7));
            $("#fieldtext8").text(defaultlabel + (limit8 - chars8));
            $("#fieldtext9").text(defaultlabel + (limit9 - chars9));

            $(function(){

                $('#specialselect').change(function() {
            
                    if($("#specialselect option:selected" ).text() == "Other") {

                        $("#selectorbox").append('<input id="textfieldreplace" maxlength="20" name="Language" type="text" onkeydown="keyup()">');

                        $("#selectorbox").append('<p class="labeltext" id="fieldtext5">Characters Left: 20</p>');


                    } 

                    else {

                        $("#textfieldreplace").remove();
                        $("#fieldtext5").remove();
                    }

               });

                $('#specialselect2').change(function() {
            
                    if($("#specialselect2 option:selected" ).text() == "Other") {

                        $("#selectorbox2").append('<input id="textfieldreplace2" maxlength="15" name="Language" type="text" onkeydown="keyup2()">');

                        $("#selectorbox2").append('<p class="labeltext" id="fieldtext9">Characters Left: 15</p>');


                    } 

                    else {

                        $("#textfieldreplace2").remove();
                        $("#fieldtext9").remove();
                    }

               });

                $('#areatextfield1').keyup(function() {

                arealimit = parseInt($(this).attr('maxlength'));  
                areatext = $(this).val();  
                areachars = areatext.length;  

                $("#areatext1").text(defaultlabel + (arealimit - areachars));

               });
               
               $('#textfield1').keyup(function() {

                limit1 = parseInt($(this).attr('maxlength'));  
                text1 = $(this).val();  
                chars1 = text1.length;  

                $("#fieldtext1").text(defaultlabel + (limit1 - chars1));

               });

               $('#textfield2').keyup(function() {

                limit2 = parseInt($(this).attr('maxlength'));  
                text2 = $(this).val();  
                chars2 = text2.length;  

                $("#fieldtext2").text(defaultlabel + (limit2 - chars2));

               });

               $('#textfield3').keyup(function() {

                limit3 = parseInt($(this).attr('maxlength'));  
                text3 = $(this).val();  
                chars3 = text3.length;  

                $("#fieldtext3").text(defaultlabel + (limit3 - chars3));

               });

               $('#textfield4').keyup(function() {

                limit4 = parseInt($(this).attr('maxlength'));  
                text4 = $(this).val();  
                chars4 = text4.length;  

                $("#fieldtext4").text(defaultlabel + (limit4 - chars4));

               });
               
               $('#textfield5').keyup(function() {

                limit5 = parseInt($(this).attr('maxlength'));  
                text5 = $(this).val();  
                chars5 = text5.length;  

                $("#fieldtext5").text(defaultlabel + (limit5 - chars5));

               });
                
               $('#textfield6').keyup(function() {

                limit6 = parseInt($(this).attr('maxlength'));  
                text6 = $(this).val();  
                chars6 = text6.length;  

                $("#fieldtext6").text(defaultlabel + (limit6 - chars6));

               });

               $('#textfield7').keyup(function() {

                limit7 = parseInt($(this).attr('maxlength'));  
                text7 = $(this).val();  
                chars7 = text7.length;  

                $("#fieldtext7").text(defaultlabel + (limit7 - chars7));

               });

               $('#textfield8').keyup(function() {

                limit8 = parseInt($(this).attr('maxlength'));  
                text8 = $(this).val();  
                chars8 = text8.length;  

                $("#fieldtext8").text(defaultlabel + (limit8 - chars8));

               });
               
               $('#textfield9').keyup(function() {

                limit9 = parseInt($(this).attr('maxlength'));  
                text9 = $(this).val();  
                chars9 = text9.length;  

                $("#fieldtext9").text(defaultlabel + (limit9 - chars9));

               });
               

            });

        });

        

        function keyup() {

            $('#textfieldreplace').keyup(function() {

                var defaultlabel = "Characters Left: ";

                limit5 = parseInt($(this).attr('maxlength'));  
                text5 = $(this).val();  
                chars5 = text5.length;  

                $("#fieldtext5").text(defaultlabel + (limit5 - chars5));

               });

        }

        function keyup2() {

            $('#textfieldreplace2').keyup(function() {

                var defaultlabel = "Characters Left: ";

                limit9 = parseInt($(this).attr('maxlength'));  
                text9 = $(this).val();  
                chars9 = text9.length;  

                $("#fieldtext9").text(defaultlabel + (limit9 - chars9));

                $("#textfield9").val($("#textfieldreplace2").val());

               });

        }

        </script>
        
        {!! Html::style('style.css') !!}
        
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Encode Thesis Data
                </div>

                <div id="line">
                </div>

                <div id="containerArea">

                {!! Form::open(array('action' => 'MasterController@datamaker')) !!}

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext"></p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        <p class="designertext"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Area/Field</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::select('Area', $Area) !!}
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Author</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::text('Author', $AUTHOR, array('id' => 'textfield1', 'maxlength' => 500 )) !!}
                        <p class="labeltext" id="fieldtext1">Characters Left: 500</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Title</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::text('Title', $TITLE, array('id' => 'textfield2', 'maxlength' => 500 )) !!}
                        <p class="labeltext" id="fieldtext2">Characters Left: 500</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Year</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::input('date', 'Year', $YEAR, array('id' => 'textfield3', 'maxlength' => 6 )) !!}
                        <p class="labeltext" id="fieldtext3">Characters Left: 6</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Advisers</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::text('Advisers', $ADVISERS, array('id' => 'textfield4', 'maxlength' => 500 )) !!}
                        <p class="labeltext" id="fieldtext4">Characters Left: 500</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Language Code</p>
                        </div>
                        <div class="col-md-4" id="formelement">

                            {!! Form::hidden('Language', $LANGUAGE, array('id' => 'textfield5', 'maxlength' => 20 )) !!}
                            {!! Form::select('LanguageSelect', $LANGUAGESELECT, 'default', array('id' => 'specialselect')) !!}

                        </div>

                        <div class="col-md-4" id="formelement">
                        <div id='selectorbox'>
                        </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Accession Numbers</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::text('AccessionNumber', $ACCESSIONNUMBER, array('id' => 'textfield6', 'maxlength' => 12 )) !!}
                        <p class="labeltext" id="fieldtext6">Characters Left: 12</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Subjects</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::text('Subjects', $SUBJECTS, array('id' => 'textfield7', 'maxlength' => 1000 )) !!}
                        <p class="labeltext" id="fieldtext7">Characters Left: 1000</p>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Abstract</p>
                        </div>
                        <div class="col-md-4" id="formelement">
                        {!! Form::textarea('Abstracts', $ABSTRACTS, array('id' => 'areatextfield1', 'maxlength' => 25000 )) !!}
                        <p class="labeltext" id="areatext1">Characters Left: 25000</p>
                        </div>
                        </div>

                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Physical Description</p>
                        </div>
                        <div class="col-md-4" id="formelement">

                        {!! Form::text('PhysicalDescription', $PHYSICALDESCRIPTION, array('id' => 'textfield8', 'maxlength' => 200 )) !!}
                        <p class="labeltext" id="fieldtext8">Characters Left: 200</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        <p class="designertext">Dissertation/Thesis Note</p>
                        </div>
                        <div class="col-md-4" id="formelement">

                        {!! Form::hidden('GeneralNote', $GENERALNOTE, array('id' => 'textfield9', 'maxlength' => 15 )) !!}
                        {!! Form::select('ThesisType', $THESISTYPE, 'default', array('id' => 'specialselect2')) !!}

                        </div>

                        <div class="col-md-4" id="formelement">
                        <div id='selectorbox2'>
                        </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-3" id="formelement">
                        {!! Form::submit('Add', ['class' => 'buttonite']) !!}
                        </div>
                    </div>
                </div>

                <a href="dataview" class="buttonite">Go Back</a>

                {!! Form::close() !!}

                </div>
            </div>
        </div>
    </body>
</html>