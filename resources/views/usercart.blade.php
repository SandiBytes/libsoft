<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        {!! Html::style('style.css') !!}

        <script>
            function printThis() {
                window.print();
                //window.location.href="mailto:?subject="+document.title+"&body="+escape(window.location.href);
            }
        </script>

    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                Your Cart
                </div>

                <div id="line">
                </div>
                <table>
                <tr>
                <td>
                {!! Form::open(array('action' => 'MasterController@send')) !!}
                </td>
                <td>
                <a href="printall" class="buttonitelarge">Print Cart</a>
                </td>
                <td>
                {!! Form::submit('Send to Email', ['class' => 'buttonite']) !!}
                </td>
                <td>
                {!! Form::email('Emailer', null, array('id' => 'emailer')) !!}
                </td>
                <td>
                <p class="designertext">Email Address</p>
                </td>

                {!! Form::close() !!}

                <td>
                {!! Form::open(array('action' => 'MasterController@usercartsubtract')) !!}
                </td>
                <td>
                {!! Form::submit('Remove Checked Items', ['class' => 'buttonite']) !!}
                </td>
                <td>
                <a href="clearcart" class="buttonitelarge">Clear Cart</a>
                </td>
                <td>
                <a href="userface2" class="buttonitelarge">Go Back</a>
                </td>

                </tr>
                </table>

                <div class="windowstyle">
                    <table class="formstyle2">
                    <tr>
                    <td>
                    <p class="designertext">View Details</p>
                    </td>
                    <td>
                    <p class="designertext">Remove from Cart</p>
                    </td>
                    <td>
                    <p class="designertext">Author</p>
                    </td>
                    <td>
                    <p class="designertext">Title</p>
                    </td>
                    <td>
                    <p class="designertext">Year</p>
                    </td>
                    <td class="firstlimiter">
                    <p class="designertext">Abstract Snippet</p>
                    </td>
                    <td style="width:1px!;!important">
                    </td>
                    </tr>

                    @foreach ($name as $name)
                    <tr>
                    <td>
                    <!--<a href="#" class="buttonite">Go to Details</a>-->
                    <a href="usercartdetails/{{($name -> id)}}" class="buttonite">Details</a>

                    </td>

                    <td>

                    <div class="limiter">{!!Form::checkbox($name -> id, $name -> id, null, ['class' => 'checkbox'])!!}</div>

                    </td>

                    <td>
                    <div class="limiter">{!!Form::label($name -> Author)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label('Title', $name -> Title)!!}</div>
                    </td>
                    <td>
                    <div class="limiter">{!!Form::label($name -> Year)!!}</div>
                    </td>
                    <td>
                    <div class="thirdlimiter">{!!Form::label('abstracts', $name -> Abstracts)!!}</div>
                    </td>
                    <td style="width:1px!;!important">
                    </td>
                    </tr>
                    @endforeach
                    </table>

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </body>
</html>
